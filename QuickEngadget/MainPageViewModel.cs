﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using Microsoft.Phone.Controls;

namespace Engadget
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private Visibility verticalyButtonOrientation;
        public Visibility VerticalyButtonOrientation
        {
            get { return verticalyButtonOrientation; }
            set 
            { 
                verticalyButtonOrientation = value;
                OnPropertyChanged(new PropertyChangedEventArgs("VerticalyButtonOrientation"));
            }
        }

        private Visibility horizontalyButtonOrientation;
        public Visibility HorizontalyButtonOrientation
        {
            get { return horizontalyButtonOrientation; }
            set 
            { 
                horizontalyButtonOrientation = value;
                OnPropertyChanged(new PropertyChangedEventArgs("HorizontalyButtonOrientation"));
            }
        }

        public MainPageViewModel(PageOrientation o)
        {
            SetButtonsVisibility(o);
        }

        public void SetButtonsVisibility(PageOrientation o)
        {
            switch (o)
            {
                case PageOrientation.Landscape:
                case PageOrientation.LandscapeLeft:
                case PageOrientation.LandscapeRight:
                    HorizontalyButtonOrientation = Visibility.Visible;
                    VerticalyButtonOrientation = Visibility.Collapsed;
                    break;

                default:
                case PageOrientation.Portrait:
                case PageOrientation.PortraitDown:
                case PageOrientation.PortraitUp:
                    HorizontalyButtonOrientation = Visibility.Collapsed;
                    VerticalyButtonOrientation = Visibility.Visible;
                    break;
            }


        }


        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

    }
}
