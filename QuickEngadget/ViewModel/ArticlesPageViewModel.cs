﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;

using Engadget.Model;

namespace Engadget.ViewModel
{
    public class ArticlesPageViewModel : INotifyPropertyChanged
    {
        public const string ENGADGET_PAGE_TYPE  = "eng";
        public const string MOBILE_PAGE_TYPE    = "mob";
        public const string HD_PAGE_TYPE        = "hd";
        public const string ALT_PAGE_TYPE       = "alt";


        public event PropertyChangedEventHandler PropertyChanged;

        private string type;
        public string Type
        {
            get { return type; }
            set
            {
                type = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Type"));
            }
        }

        private GetFeed getFeed;

        private string titleImageLink;
        public string TitleImageLink
        {
            get { return titleImageLink;  }
            set 
            {
                titleImageLink = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TitleImageLink"));
            }
        }

        private Visibility loadingStackVisible;
        public Visibility LoadingStackVisible
        {
            get { return loadingStackVisible; }
            set
            {
                loadingStackVisible = value;
                OnPropertyChanged(new PropertyChangedEventArgs("LoadingStackVisible"));
            }
        }

        private ObservableCollection<Article> articles;
        public ObservableCollection<Article> Articles
        {
            get { return articles; }
            set { articles = value; }
        }

        public ArticlesPageViewModel(string type)
        {
            if (type == null)
                type = "";

            Type = type;
            Articles = new ObservableCollection<Article>();

            SetLinks();
        }

        private void SetLinks()
        {
            switch(Type)
            {
                default:
                case ENGADGET_PAGE_TYPE:
                    TitleImageLink = "../Images/eng.png";
                    getFeed = new GetFeed(this, App.ENGADGET_URL);
                    break;

                case MOBILE_PAGE_TYPE:
                    TitleImageLink = "../Images/eng_mobile.png";
                    getFeed = new GetFeed(this, App.ENGADGET_MOBILE_URL);
                    break;

                case HD_PAGE_TYPE:
                    TitleImageLink = "../Images/eng_hd.png";
                    getFeed = new GetFeed(this, App.ENGADGET_HD_URL);
                    break;

                case ALT_PAGE_TYPE:
                    TitleImageLink = "../Images/eng_alt.png";
                    getFeed = new GetFeed(this, App.ENGADGET_ALT_URL);
                    break;
            }
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }
    }
}
