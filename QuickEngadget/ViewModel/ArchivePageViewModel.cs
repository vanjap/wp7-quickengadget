﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Engadget.Model;

namespace Engadget.ViewModel
{
    public class ArchivePageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Article> Engadget { get; set; }
        public ObservableCollection<Article> Mobile { get; set; }
        public ObservableCollection<Article> Hd { get; set; }
        public ObservableCollection<Article> Alt { get; set; }

        public ArchivePageViewModel()
        {
            Engadget = new ObservableCollection<Article>();
            Mobile = new ObservableCollection<Article>();
            Hd = new ObservableCollection<Article>();
            Alt = new ObservableCollection<Article>();

            Engadget = Engadget.LoadArticles(App.ENGADGET_ARCHIVE_FILE);
            Mobile = Mobile.LoadArticles(App.ENGADGET_MOBILE_ARCHIVE_FILE);
            Hd = Hd.LoadArticles(App.ENGADGET_HD_ARCHIVE_FILE);
            Alt = Alt.LoadArticles(App.ENGADGET_ALT_ARCHIVE_FILE);
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }
    }
}
