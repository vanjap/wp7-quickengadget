﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Windows.Navigation;

using Engadget.Model;
using Engadget.ViewModel;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace Engadget.View
{
    public partial class ArticlesPage : PhoneApplicationPage
    {
        private ArticlesPageViewModel viewModel;

        public ArticlesPage()
        {
            InitializeComponent();

            App.articlesPage = this;
        }

        protected override void OnNavigatedTo(NavigationEventArgs args)
        {
            switch (App.NavigatedFrom)
            {
                case (int)App.NavigatedFromPages.MainPage:
                    OnNavigatedFromMainPage();
                    break;
                
                case (int)App.NavigatedFromPages.ArticlePage:
                    FreeSelection();
                    break;
            }
            

            base.OnNavigatedTo(args);
        }

        private void OnNavigatedFromMainPage()
        {
            IDictionary<string, string> parameters = this.NavigationContext.QueryString;
            string type = null;

            if (parameters.ContainsKey("type"))
            {
                type = parameters["type"];
            }

            viewModel = new ArticlesPageViewModel(type);

            DataContext = viewModel;
            ArticlesList.ItemsSource = viewModel.Articles;
        }


        private void ArticlesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem selectedItem = this.ArticlesList.ItemContainerGenerator.ContainerFromItem(this.ArticlesList.SelectedItem) as ListBoxItem;
            if (selectedItem == null)
                return;

            App.articleToDisplay = (Article)selectedItem.Content;
            NavigationService.Navigate(new Uri("/View/ArticlePage.xaml", UriKind.Relative));
        }

        private void FreeSelection()
        {
            try
            {
                ArticlesList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        private void OnArchiveMenuClick(object sender, RoutedEventArgs e)
        {
            ListBoxItem selectedListBoxItem = ArticlesList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            if (selectedListBoxItem == null)
                return;

            Article item = (Article)selectedListBoxItem.Content;

            ObservableCollection<Article> archive = new ObservableCollection<Article>();
            archive.AddArticle(item, viewModel.Type);
        }

    }
}