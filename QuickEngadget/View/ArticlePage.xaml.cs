﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.Windows.Navigation;
using Engadget.Model;

namespace Engadget.View
{
    public partial class ArticlePage : PhoneApplicationPage
    {

        public ArticlePage()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(ArticlePage_Loaded);
        }


	    public bool IsDarkTheme()
	    {
	        bool isDarkTheme;
	        isDarkTheme = (Visibility.Visible==(Visibility)Application.Current.Resources["PhoneDarkThemeVisibility"]);
	        return isDarkTheme;
	    }

        private void ArticlePage_Loaded(object sender, RoutedEventArgs args)
        {
            if (App.articleToDisplay != null)
            {
                string head = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1250\" /></head>";
                string end = "</font></body></hmtl>";
                string start;

                if (IsDarkTheme())
                {
                    start = head + "<body bgcolor=\"#000000\"><font color = \"#FFFFFF\">";
                }
                else
                {
                    start = head + "<body bgcolor=\"#FFFFFF\"><font color = \"#000000\">";

                }
                
                this.ContentWebBrowser.NavigateToString(start + App.articleToDisplay.Content + end);
                this.PageTitle.Text = App.articleToDisplay.Title;
            }

            App.NavigatedFrom = (int)App.NavigatedFromPages.ArticlePage;

        }

    }
}